!   EXAMPLE (Integrate a real function of one variable over a finite interval)
!
!     The problem is to integrate the function
!
!     f(x) = (x**2)*(x**2-2.d0)*sin(x)
!
!     over the interval a to b
!
!     Output from the DGAUS8 subroutine is compared with the exact solution.
!
!     ##########
!
program test_dgaus8
    implicit none
    integer i, ierr
    double precision a, b, err, ans, exact, delta
    double precision, external :: fun
    double precision funt, x
    funt(x) = 4.d0*x*((x**2)-7.d0)*sin(x)-((x**4)-14.d0*(x**2)+28.d0)*cos(x)

    a = 0.d0
    b = 0.d0
    do i = 1, 12
       err = 1.0d06
       call DGAUS8 (FUN, A, B, ERR, ANS, IERR)
       exact = funt(b) - funt(a)
       delta = ans - exact
       write(6,*)
       write(6,'(A20,F16.12)') 'Lower limit         ', A
       write(6,'(A20,F16.12)') 'Upper limit         ', B
       write(6,'(A20,F16.12)') 'Solution from DGUAS8', ANS
       write(6,'(A20,F16.12)') 'Exact solution      ', exact
       write(6,'(A20,F16.12)') 'Delta               ', delta
       b = b + 4.d0*atan(1.d0)/12.d0
    end do
end program test_dgaus8

double precision function fun(x)
  double precision x
  fun = (x**2)*(x**2-2.d0)*sin(x)
end function fun
