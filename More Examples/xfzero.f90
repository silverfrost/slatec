! Example program, root of nonlinear eq. in one variable
! See http://www.cs.yorku.ca/~roumani/fortran/slatecAPI/fzero.f.html
!
module global_c
   implicit none
   real Ste            ! parameter in equation being solved
end module

program xfzero         ! example program for SLATEC routine Fzero
   use global_c
   implicit none
   real b,c,r,relerr,abserr
   integer iflag
   real, external :: func

   Ste = 0.3           ! set value of parameter in nonlinear equation
   r   = sqrt(Ste/2)   ! approximate solution
   b   = 0.0           ! search interval (b, c)
   c   = 1.0
   print *,'trial root = ',r
   relerr = 0.001
   abserr = 0.0005

   call fzero(func, b, c, r, relerr, abserr, iflag)

   if (iflag .eq. 1) then
      print *,' Approx. root found: ', b
      print *,' func(root) value = ', func(b)
   else
      print *,' Iflag = ',iflag
   endif

end program

real function func(x)
   use global_c              ! module makes equation parameter available
   implicit none
   real x
   real, external :: erf

   func = sqrt(acos(-1.0))*x*exp(x*x)*erf(x) - Ste  !Stefan's equation

   return
end function


