! 7. EXAMPLE (Simultaneous nonlinear equation solution).
!
!       The problem is to determine the values of x(1), x(2), ..., x(9),
!       which solve the system of tridiagonal equations
!
!       (3-2*X(1))*X(1)           -2*X(2)                   = -1
!               -X(I-1) + (3-2*X(I))*X(I)         -2*X(I+1) = -1, I=2-8
!                                   -X(8) + (3-2*X(9))*X(9) = -1
!     **********
!
program test
   implicit none
!
   double precision, dimension(9) :: diag, fvec, qtf, wa1, wa2, wa3, wa4, x
   double precision :: epsfcn, factor, fnorm, xtol
   double precision, dimension(9,9) :: fjac
   integer :: info, iopt, j, jac, ldfjac, lr, maxfev, ml, mode, mu,   &
            & n, nfev, njev, nprint
   integer, save :: nwrite
   double precision :: r(45)
   double precision, external :: denorm
   external :: fcn
!
!     Driver for dnsq example.
!
   data nwrite/6/
!
   iopt = 2
   n = 9
!
!     The following starting values provide a rough solution.
!
   x(1:9) = -1.D0
!
   ldfjac =  9
   lr     = 45
!
!     Set xtol to the square root of the machine precision.
!     unless high precision solutions are required,
!     this is the recommended setting.
!
   xtol = sqrt(epsilon(xtol))
!
   maxfev = 2000
   ml = 1
   mu = 1
   epsfcn = 0.d0
   mode = 2
   diag(1:9) = 1.D0
   factor = 1.D2
   nprint = 0
!
   call dnsq(fcn,jac,iopt,n,x,fvec,fjac,ldfjac,xtol,maxfev,ml,mu,epsfcn,  &
      & diag, mode,factor,nprint,info,nfev,njev,r,lr,qtf,wa1,wa2,wa3,wa4)

   fnorm = denorm(n,fvec)
   write (nwrite,99) fnorm, nfev, info, (x(j),j=1,n)
99 format (5x,' Final l2 norm of the residuals',es15.7//5x,               &
      &,16x, ' Number of function evaluations',i10//5x,' Exit parameter'  &
      & i10//5x,' Final approximate solution'//(5x,3es15.7))
   stop
end program test
!
subroutine fcn(N,X,Fvec,Iflag)
   implicit none
!
   integer :: N
   double precision, dimension(N) :: X
   double precision, dimension(N) :: Fvec
   integer :: Iflag
!
   integer :: k
   double precision, save :: one, three, two, zero
   double precision :: temp, temp1, temp2
!
   data zero, one, two, three/0.D0, 1.D0, 2.D0, 3.D0/
!
   if ( Iflag/=0 ) then
      do k = 1, N
         temp = (three-two*X(k))*X(k)
         temp1 = zero
         if ( k /= 1 ) temp1 = X(k-1)
         temp2 = zero
         if ( k /= N ) temp2 = X(k+1)
         Fvec(k) = temp - temp1 - two*temp2 + one
      enddo
      goto 99
   endif
!
!     Insert print statements here when nprint is positive.
!
 99 return
end subroutine fcn

