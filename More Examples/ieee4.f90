!   EXAMPLE (Calculate the torsional frequencies and mode shapes for the IEEE
!   1st benchmark for synchronous resonance).
!
!     The problem is to calculate the torsional frequencies and corresponding mode 
!     shapes for the IEEE 1st benchmark for synchronous resonance (1).
!
!     The parameters for this problem can be found at:
!     https://link.springer.com/content/pdf/bbm:978-1-4615-5633-6/1.pdf
!
!     The parameters are real values taken from the Navajo project which experienced
!     two significant torsional failures of the turbine generator mechanical string.
!
!     The rotor of a thermal generating unit is a complex mechanical system.  It may
!     exceed 50 metres in total length and weigh several hundred tons.  A continuum
!     model of the rotor system would be required to account for the full range of
!     rotor torsional frequencies.  The problem due to the interation between the
!     electrical power system and rotor mechanical system is principally in the
!     subsynchronous frequency range.  This allows the representation of the rotor by
!     a lumped mass model.
!
!     The lumped mass model of the mechanical string comprises of one high pressure 
!     turbine, an intermediate pressure turbine, two low, pressure turbines, the 
!     generator, and the generator's exciter.
!
!        |   |     |   |     |   |     |   |     |   |    |   |
!        |HP |=====|IP |=====|LPA|=====|LPB|=====|GEN|====|EXC|
!        |   |     |   |     |   |     |   |     |   |    |   |
!
!     As we are examining a structure with six rotating masses, there will be six different
!     torsonal modes.  These modes are numbered from 0 to 5.
!
!     The parameters for each turbine/generator section and each intermediate shaft are:
!
!     Mass    Shaft     H(s)        Shaft stiffness (pu Torque/rad)
!      HP               0.092897
!             HP-IP                 19.303
!      IP               0.155589
!             IP-LPA                34.929
!      LPA              0.858670
!             LPA-LPB               52.038
!      LPB              0.884215
!             LPB-GEN               70.858
!      GEN              0.869495
!             GEN-EXC                2.822
!      EXC              0.0342165
!
!     NOTE THAT THESE PARAMETERS ARE NOT EXPRESSED IN SI UNITS.
!
!     The inertia constant is:
!
!          H = Stored energy in rotating mass at rated speed/generator volt-ampere rating
!
!            = 0.5 * J * ( ((2*pi)*(r.p.m/60))**2 ) / VAbase
!
!     In a three phase unit:
!
!          VAbase  = sqrt(3) * rated line to line voltage * rated phase current
!
!                  =  3 * rated line to neutral voltage * rated phase current  
!
!     The stiffness constant is the per unit transmitted torque required to "twist" 
!     that shaft by one radian.
!
!     In this context:
!
!          1 pu torque = generator volt-ampere rating / rated mechanical speed in rad/s
!
!                      = VAbase / rated_omega_mech
!
!                      = VAbase * fp / (2 * rated_omega_mech)
!
!          fp is the number of magnetic poles of the field winding, which is always an 
!          even number.
!
!          A 60 Hz generator with two field poles rotates at 3600 r.p.m., while a 60 Hz 
!          generator with four field poles rotates at 1800 r.p.m.
!
!          Note that some texts use "pole pairs" rather than "poles", i.e. a 4 pole machine
!          has 2 pole pairs.
!
!
!     Calculation methodology
!
!     The eigenvector/eigenvalue matrix technique is a closed from solution that calculates 
!     the torsional natural frequencies and vibration mode shapes by directly solving the
!     differential equations of motion of the lumped mathematical model of the torsional system.
!
!     For the 1st rotating mass of the string, n = 1 and the equation of rotational motion is:
!
!      2 * H(1) * A(1)  =    K(1)*(theta(2) - theta(1))
!
!     For the rotating mass n, within the string the equation of rotational motion is:
!
!      2 * H(n) * A(n)  =   K(n-1) * (theta(n-1) - theta(n)) + K(n)*(theta(n+1) - theta(n))
!
!     For the 6th (and last) rotating mass of the string, n = 6 and the equation rotational of motion is:
!
!      2 * H(6) * A(6)  =    K(6)*(theta(5) - theta(6))
!
!     In matrix form (neglecting damping), the system of differential equations is:
!
!      2*[H]*[A] + [K]*[theta] = 0
!
!     where       [H]     = diagonal inertia matrix
!                 [A]     = angular acceleration vector (rad/sec^2)
!                 [K]     = stiffness matrix
!                 [theta] = angular displacement vector (radians)
!
!     The solution to these equations can be obtained assuming simple harmonic motion, in which case
!     the matrix equation is reduced to:
!
!                2*[H]*[omega^2]*[theta] = [K]*[theta]
!
!     where   omega^2  represents the diagonalised eigenvalue matrix.
!
!     If [HINV] is the inverse of 2*[H], the equation can be rewritten as:
!
!              {[HINV]*[K]  [omega^2]}*[theta] = 0
!
!     This form of matrix equation is the well known eigenvalue equation.  The values of [omega^2]
!     for which the equation is soluble are the eigenvalues of the matrix.  The vector solutions
!     for [theta] are the eigenvectors of the matrix product [HINV]*[K].
!
!     Physically, these eigenvectors represent the mode shape of the vibration corresponding to 
!     the eigenvalue or natural frequency.  Each eigenvector is normalised with a maximum amplitude
!     of unity to illustrate the torsional vibration mode shapes.
!
!     The modal inertia and stiffness matrices define the single degree of freedom system which
!     has the same torsional natural frequencies as each the full system's individual torsional
!     modes.
!
!     As rated speed of the turbine generator set is a parameter required to determine both H (s)
!     and shaft stiffness K ((pu Torque/rad), the electrical frequency (60 Hz) for the IEEE 
!     benchmark model, must be factored into the calculation of the torsional natural
!     frequencies. This means that:
!
!         1. The elements of the H matrix must be multiplied by two since 
!            
!            H = ( 0.5*J*(rated speed)**2 ) / VAbase
!
!            If rated speed is taken as 1 per unit and VAbase is also taken as 1 per unit then, 
!
!            J = 2*H
!
!         2. Consequently the intermediate calculated eigenvalue for each mode is not the 
!            square root of the angular frequency of the torsional mode.
!
!         3. These combine to form a slightly modified equation when calculating the torsional
!            resonance frequency.  The nominal power system frequency is introduced at this stage
!            (rated speed in rad/s being proportional to system frequency).
!
!     In this program the eigenvalues and eigenvectors are determined using SLATEC routines
!     SPOCO, SPODI and SGEEV.
!
!     The torsional frequencies reported in (1) for Modes 1 to 4 are 15.71 Hz, 
!     20.21 Hz, 25,55 Hz, and 32.28 Hz, which are replicated by this program.
!
!     The results calculated by this program are presented in both tabular and graphics format.
!
!     Referring to the graphical results:
!
!         Mode 0 at 0.000 Hz represents the solid body case.  There is no relative displacement
!         between the rotating masses and therefore no interchange of energy between the rotating
!         masses and the torsional strings i.e. the shafts linking the masses.  This is not a true
!         torsional mode.
!
!         Mode 1 at 15.712 Hz shows mass numbers 5 and 6 (the generator and its exciter), swinging
!         torsinally against the mass numbers 1, 2, and 3 (HP, IP, and LPA turbines).  Mass number
!         4 (LPB turbine) does not significantly contribute to this mode.  As the mode shape crosses
!         the displacment axis between mass numbers 3 and 4, this means that during the interchange of 
!         energy between kinetic and strain forms associated with this torsional mode, when the kinetic
!         energy is zero and the stain energy is at its maximum, most of this strain energy will be 
!         concentrated in the shaft between mass numbers 3 and 4.  At this location the torsional stress
!         will therefore be oscillating at 15.712 Hz between two extreme positive and negative values.
!
!         Mode 2 at 20.211 Hz shows mass numbers 1,2,3 and 6, swinging against 4,5.  Mass number 6, 
!         which is the genertor excitor has by far the lowest inertia of all the individual rotors.
!         The total inertia of all the other rotors (1 to 5), is so massive compared to that of mass
!         number 6, that they do not significantly deviate from the zero line during this oscillation.
!         This oscillation mode will produce high torsional stress on the shaft between mass numbers
!         5 and 6.
!
!         Mode 3 at 25.547 Hz shows masses 1, 2, and 5, swinging against 3, 4, and 6 in a snake like
!         manner.
!
!         Mode 4 at 32.285 Hz is the most significant from the point of view of the generator's 
!         participation in all these torsional oscillations, as this mode shows the highest level of
!         generator participation in all of the torsional cases.  As noted above, the data in the 
!         model is based on the Navajo project, and it was a torsional oscillation close to 30 Hz which
!         was observed on the Navojo turbine genertor before the torsional failure occured.
!
!         Mode 5 at 47.456 Hz shows the high pressure turbine swining against the intermediate turbine
!         with limited participation of all the other units.
!
!     (1) "FIRST BENCHMARK MODEL FOR COMPUTER SIMULATION OF SUBSYNCHRONOUS RESONANCE",
!         IEEE Trans. on Power Apparatus and Systems, Vol PAS-96, Sept/Oct 1977.
!       
!     ##########
      winapp
      program ieee4
      implicit none
      integer, parameter :: n=6
      real,    parameter :: eps=0.0001, pi = 4.0*atan(1.0), twopi = 2.0*pi
      !  Input data from IEEE 1st Benchmark for subsynchronous resonance
      character(len=3) :: name(n) = ['HP ','IP ','LPA','LPB','GEN','EXC']
      real :: hc(n)   = [0.092897,0.155589,0.858670,0.884215,0.868495,0.0342165]
      real :: ts(n-1) = [19.303,34.929,52.038,70.858,2.822]
      real :: freq    = 60.0
      !   End of input data from IEEE paper
      real h(n,n), k(n,n), hinv(n,n), q(n,n), hm(n,n), km(n,n), modal_f(n), &
           e_real(n), v_real(n,n), big(n), checkinv(n,n)
      real rcond, det
      real work(n), work2(2*n)
      complex e(n), v(n,n)
      integer i, j, info, job, ersort(n)
      ! Following are associated with Step 14
      double precision mode0(n),mode1(n),mode2(n),mode3(n),mode4(n),mode5(n)
      character(len=240) plstring
      character(len=40) titlestring(n)
!     USES SPOCO, SPODI, SGEEV

!     Executable code starts here

!     Step 1 - Form h (inertia) matrix and print to screen
!              The factor of 2 comes from the use of the H constant
!              rather than inertia. h is a diagonal matrix.
      h = 0.d0
      forall (i=1:n) h(i,i) = 2.0*hc(i)

      write(6,*)
      write(6,'(1x,a)') 'H (inertia) matrix (s).'
      do i = 1, n
        write(6,'(6(F13.6,1X))') (h(i,j), j = 1,n, 1)
      end do
      
!     Step 2 - Form k (stiffness) matrix and print to screen
!              The i and i+1 offsets come from the equations of rotational
!              motion described above.  k is a banded diagonal matrix.
      k = 0.d0
      do i = 1, n-1, 1
        k(i,   i  ) = k(i,   i  ) + ts(i)
        k(i,   i+1) = k(i,   i+1) - ts(i)
        k(i+1, i  ) = k(i+1, i  ) - ts(i)
        k(i+1, i+1) = k(i+1, i+1) + ts(i)
      end do

      write(6,*)
      write(6,'(1x,a)') 'K (stiffness) matrix (pu Torque/rad).'
      do i = 1, n
        write(6,'(6(F13.6,1X))') (k(i,j), j = 1,n, 1)
      end do
      
!     Step 3 - Invert h matrix using the Slatec routines SPOCO and SPODI

      hinv = h
      CALL SPOCO(hinv,n,n,rcond,work,0)
      job = 1
      CALL SPODI(hinv,n,n,det,job)
      
      write(6,*)
      write(6,'(1x,a)') 'Inverse of H matrix (1/s)'
      do i = 1, n
        write(6,'(6(F13.6,1X))') (hinv(i,j), j = 1,n, 1)
      end do

!     Check that the matrix product of H and HINV is unit matrix
      checkinv = matmul(h,hinv)
      write(6,*)
      write(6,'(1x,a)') 'Matrix product of H and inverse of H.'
      do i = 1, n
        write(6,'(6(F13.6,1X))') (checkinv(i,j), j = 1,n, 1)
      end do

!     Step 4 - Form q matrix equal to matmul(h_inv,k) and print to screen

      q = matmul(hinv,k)
      write(6,*)
      write(6,'(1x,a)') 'Q matrix (pu Torque/rad/s)  Q is the matrix product of the inverse of H and K.'
      do i = 1, n
        write(6,'(6(F13.6,1X))') (q(i,j), j = 1,n, 1)
      end do
      
!     Step 5 - Now find eigenvalues and eigenvectors of q using Slatec routine SGEEV

      job = 1
      CALL SGEEV(q, n, n, e, v, n, WORK2, JOB, INFO)
      
      write(6,*)
      write(6,'(1x,a)') 'Eigenvalue vector of Q as returned by SGEEV'
      do i = 1, n
        write(6,'(1x,i3,1x,"(",f7.3,",",f6.3,")")') i, e(i)
      end do

      write(6,*)
      write(6,'(1x,a)') 'Eigenvector matrix of Q as returned by SGEEV'
      do i = 1, n
        write(6,'(6(1x,"(",f7.3,",",f6.3,")"))') (v(i,j), j = 1,n)
      end do

!     Step 6 - Neglect the zero imaginary parts of e and v.

!              We will instead work with reals i.e. e_real = real(e) and 
!              v_real = real(v), to simplify the following code

      e_real = real(e)
      v_real = real(v)

!     Step 7 - Use Silverfrost routine RSORT@ so that we can list eigenvalues in ascending order

!              Here RSORT@ sorts the REAL array e_real by setting pointers from 1 to N in the array ersort.

      call RSORT@(ersort, e_real, n)
      
      write(6,*)
      write(6,'(1x,a)') 'Eigenvalues of Q listed in ascending mode order'
      do i = 1, n
        write(6,'(1x,a,1x,i3,1x,f10.3)') 'Mode',i-1, e_real(ersort(i))
      end do
      
!     Step 8 - Normalise the eigenvector matrix

!              The elements of each column are scaled so that the largest element in a column is
!              positive or negative one.

      big = max(abs(maxval((v_real),dim=1)), abs(minval((v_real),dim=1)))
      do i = 1, n
         forall (j =1:n) v_real(j,i) = v_real(j,i)/big(i)
      end do

      write(6,*)
      write(6,'(1x,a)') 'Normalised eigenvectors matrix'
      write(6,'(1x,a)') '(in columns with increasing mode number left to right)'
      do i = 1, n
        write(6,'(6(1x,f10.3))') (v_real(i,ersort(j)), j = 1,n)
      end do
      
!     Step 9 - Form modal inertia matrix

      hm = matmul(transpose(v_real), matmul(h,v_real))
      
!     Step 10 - Form modal stiffness matrix

      km = matmul(transpose(v_real), matmul(k, v_real))
      
!     Step 11 - Do this to avoid very small negative numbers which cause problems with SQRT later

      where (abs(hm) < eps) hm = 0.0
      where (abs(km) < eps) km = 0.0
      
!     Step 12 - Calculate the modal frequencies

      forall(i=1:n) modal_f(i) = sqrt((freq*twopi*km(i,i)/hm(i,i)))/(twopi)
      
!     Step 13 - Output the final results

      write(6,*)
      write(6,'(1x,a)') 'Torsional frequencies and normalised mode shapes for IEEE 1st benchmark.'
      write(6,*)
      write(6,'(1x,a)') 'Table 1 - Unsorted'
      write(6,*)
      write(6,'(1x,a,2x,a)')            'Frequency [Hz]',' ------------- Mode shape ----------------'
      write(6,'(1x,a,1x,6(a6,1x))')      '--------------', (name(i),i=1,n)
      do i = 1, n
        write(6,'(1x,1x,f10.3,6x,6(f6.3,1x))') modal_f(i),(v_real(j,i), j = 1,n)
      end do
      write(6,*)
      write(6,'(1x,a)') 'Table 2 - Sorted by mode number'
      write(6,*)
      write(6,'(1x,a,1x,a)')            'Modal frequency [Hz]',' ------------- Mode shape ----------------'
      write(6,'(1x,a,1x,6(a6,1x))')      '-------------------', (name(i),i=1,n)
      do i = 1, n
        write(6,'(1x,a,1x,i3,1x,f10.3,3x,6(f6.3,1x))') 'Mode',i-1, modal_f(ersort(i)),(v_real(j,ersort(i)), j = 1,n)
      end do

!     Step 14 Plot the results in graphical form using Clearwin+

      do i = 1, n
        mode0(i) = v_real(i,ersort(1))
        mode1(i) = v_real(i,ersort(2))
        mode2(i) = v_real(i,ersort(3))
        mode3(i) = v_real(i,ersort(4))
        mode4(i) = v_real(i,ersort(5))
        mode5(i) = v_real(i,ersort(6))
        write(titlestring(i),'(a,i2,a,f6.3,a)') 'Title="Mode', i-1, ' Frequency ', modal_f(ersort(i)), ' Hz"'
      end do
      i = winio@('%cn%bfNatural torsional frequencies and normalised mode shapes (eigenvectors) for IEEE 1st benchmark%2nl&')
      plstring='%pl[native,n_graphs=1,link=lines,width=2,y_min=-1,y_max=1,dx=1,dy=0.5,frame,margin=50,'&
               //'symbol=13,gridlines,smoothing=4,xaxis="Mass Number",yaxis="Displacement",'
      i = winio@('%fn[Consolas]%ts&',1.2d0)
      i = winio@(plstring//titlestring(1)//']&',   500,250,n,1.d0,1.d0,mode0)
      i = winio@(plstring//titlestring(2)//']%ff&',500,250,n,1.d0,1.d0,mode1)
      i = winio@(plstring//titlestring(3)//']&',   500,250,n,1.d0,1.d0,mode2)
      i = winio@(plstring//titlestring(4)//']%ff&',500,250,n,1.d0,1.d0,mode3)
      i = winio@(plstring//titlestring(5)//']&',   500,250,n,1.d0,1.d0,mode4)
      i = winio@(plstring//titlestring(6)//']&',   500,250,n,1.d0,1.d0,mode5)
      i = winio@('')
      end program ieee4
