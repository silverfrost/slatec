!  http://www.cs.yorku.ca/~roumani/fortran/slatecAPI/keylist.index.html
!       The problem is to determine the values of x(1), x(2), ..., x(9),
!       which solve the system of tridiagonal equations
!
!       (3-2*X(1))*X(1)           -2*X(2)                   = -1
!               -X(I-1) + (3-2*X(I))*X(I)         -2*X(I+1) = -1, I=2-8
!                                   -X(8) + (3-2*X(9))*X(9) = -1
!
!       **********
! Powell's hybrid method, "easy interface version".
!
program test
   implicit none
!
   double precision :: fnorm , tol
   double precision , dimension(9) :: fvec , x
   integer :: info , iopt , j , jac , lwa , n , nprint
   integer , save :: nwrite
   double precision , dimension(180) :: wa
   double precision, external :: denorm
   external :: fcn, jac
!
!     Driver for dnsqe example.
!
   data nwrite/6/
!
   iopt = 2
   n = 9
!
!     The following starting values provide a rough solution.
!
   do j = 1 , 9
      x(j) = -1.D0
   enddo

   lwa = 180
   nprint = 0
!
!     Set tol to the square root of the machine precision.
!     unless high precision solutions are required,
!     this is the recommended setting.
!
   tol = sqrt(epsilon(tol))
!
   call dnsqe(fcn,jac,iopt,n,x,fvec,tol,nprint,info,wa,lwa)
   fnorm = denorm(n,fvec)
   write (nwrite,99001) fnorm , info , (x(j),j=1,n)
99001 format (5x,' Final l2 norm of the residuals',es15.7//5x,' Exit parameter', &
            & 16x,i10//5x,' Final approximate solution'//(5x,3es15.7))
   stop
end program test
!
subroutine fcn(N,X,Fvec,Iflag)
   implicit none
!
! Dummy argument declarations rewritten by SPAG
!
   integer :: N
   double precision , dimension(N) :: X
   double precision , dimension(N) :: Fvec
   integer :: Iflag
!
   integer :: k
   double precision , save :: one , three , two , zero
   double precision :: temp , temp1 , temp2
!
   data zero , one , two , three/0.D0 , 1.D0 , 2.D0 , 3.D0/
!
   do k = 1 , N
      temp = (three-two*X(k))*X(k)
      temp1 = zero
      if ( k/=1 ) temp1 = X(k-1)
      temp2 = zero
      if ( k/=N ) temp2 = X(k+1)
      Fvec(k) = temp - temp1 - two*temp2 + one
   enddo
end subroutine fcn

subroutine jac()  ! Dummy routine
stop 'JAC was entered'
end subroutine jac

