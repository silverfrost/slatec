! Solution of system of nonlinear equations, Brown's Method, ACM TOMS 316
! http://www.cs.yorku.ca/~roumani/fortran/slatecAPI/sos.f.html
program tsos
   implicit none
   real, external :: fnc
   integer neq, iflag, liw, lrw
   integer, allocatable :: iw(:)
   real, allocatable :: x(:), rw(:)
   real :: rtolx, atolx, tolf

   neq = 9
   lrw = neq*(6+(neq+1)/2)+10
   liw = neq+3
   allocate (x(neq), rw(lrw), iw(liw))

   x = -1.0    ! initial guess
   rtolx = 1e-4
   atolx = 1e-3
   tolf  = 1e-2
   iflag = 0      ! 0 for defaults

   call sos(fnc, neq, x, rtolx, atolx, tolf, iflag, rw, lrw, iw, liw)

   print 5, iflag, rw(1), iw(3)
 5 format('SOS returned iflag = ',i2,' residual norm = ',es10.3, &
       ' after ',i2,' iterations')
   print 10, x

   deallocate(rw,iw,x)

 10 format('Solution : ',5ES12.4,/,11x,4ES12.4)
end program

real function fnc(x, k)     ! return value of k-th function
   real x(*)
   integer k
   double precision :: temp, temp1, temp2
!
   temp = (3-2*X(k))*X(k)
   select case(k)
   case (1)
      temp1 = 0
      temp2 = x(k+1)
   case (9)
      temp1 = x(k-1)
      temp2 = 0
   case default
      temp1 = x(k-1)
      temp2 = x(k+1)
   end select
   Fnc = temp - temp1 - 2*temp2 + 1
   return

end function fnc
