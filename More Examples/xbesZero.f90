!   EXAMPLE (Use of Slatec routines BESJ0 and FZERO)
!
!     The problem is to find the first ten zeros of:
!
!     f(x) = BESJ0(x)    for x > 0.0
!
!     ##########
      program xbesj0fzero
      implicit none
      integer iflag,nroots
      real  b,c,r,abserr,relerr,bstart,cstart,dx
      real, external :: besj0
      external       :: fzero
      relerr = 0.0001
      abserr = 0.00005
      dx     = 0.1
      bstart = 0.0
      cstart = bstart + dx
      nroots = 0
      write(6,'(1x,a)') 'Roots of besj0(x):'
      write(6,'(1x,t17,a,t27,a)') 'x','f(x)'
      ! Begin search
      do while (nroots .lt. 10)
        b = bstart
        c = cstart
        call FZERO(BESJ0,b,c,r,relerr,abserr,iflag)
        ! Exammine iflag to determine next step
        if (iflag .eq. 4) then
            ! No change in sign of F(X) was found - expand search range
            cstart = cstart + dx
            cycle
        else if (iflag .eq. 1) then
            ! B is within the requested tolerance of a zero.
            nroots = nroots + 1
            write(6,'(1x,a,i3,2(4x,2f10.4))') 'Root', nroots, b, BESJ0(b)
            ! Now set search range for next root beyond b
            bstart = b + dx
            cstart = bstart + dx
            cycle
        else if (iflag .eq. 5) then
            STOP 'Too many iterations in FZERO'
        end if
      end do
      end program xbesj0fzero
