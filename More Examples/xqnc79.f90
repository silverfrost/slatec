! Example program, quadrature, SLATEC QNC79
! See http://www.cs.yorku.ca/~roumani/fortran/slatecAPI/qpdoc.f.html
!
program xqnc79
   implicit none
   real a,err,b,result
   integer ierr, nevals
   real, external    :: func
   a = 0.0e0          ! lower limit
   b = acos(0.0)      ! upper limit, Pi/2
   err = 1e-5

   call qnc79 (func,a,b,err,result,ierr, nevals)              ! Adaptive Newton-Cotes 7-point

   print *,'Return code from QNC79: ',ierr
   if (ierr /= 1) then
      print *, 'Error occurred in QNC79, see documentation for meaning of error code'
   else
      print *,'   Value of Integral = ',result, ' after ',nevals,' function evaluations'
   endif
   stop
end program
!
real function func(x)      ! Integrand from Film Condensation
   implicit none           ! Maple: evalf(int((x/sin(x))^(1/3),x=0..Pi/2));
   real x                  !        gives 1.650062725
   if (x.gt.0.0e0) then
      func = (x/sin(x))**(1.0/3)
   else
      func = 1.0e0
   endif
   return
end function

