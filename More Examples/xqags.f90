! Example program, quadrature, singular integrand at limits, SLATEC QAGS
! See http://www.cs.yorku.ca/~roumani/fortran/slatecAPI/qpdoc.f.html
!
program xqags
   implicit none
   real a,abserr,b,epsabs,epsrel,result
   integer ier,last,lenw,limit,neval
   integer, allocatable :: iwork(:)
   real, allocatable :: work(:)
   real, external :: func
   a = 0.0e0          ! lower limit
   b = acos(-1.0)     ! upper limit, Pi
   epsabs = 0.0e0
   epsrel = 1.0e-5
   limit = 100        ! max. number of subintervals
   lenw = limit*4
   allocate (iwork(limit), work(lenw))

   call qags(func,a,b,epsabs,epsrel,result,abserr,neval, &   ! SLATEC QUADPACK integrator
       ier,limit,lenw,last,iwork,work)                       ! for singular integrands

   print *,'Return code from QAGS: ',ier
   if (ier /= 0) then
      print *, 'Error occurred in QAGS, see documentation for meaning of error code'
   else
      print *,'   Value of Integral = ',result
   endif
   deallocate(iwork, work)
   stop
end
!
real function func(x)   ! Integrand from Film Condensation
   implicit none
   real x               ! Maple: evalf(int((x/sin(x))^(1/3),x=0..Pi));
   func = 0.0e0         !        gives 4.4900085011
   if(x.gt.0.0e0) func = (x/sin(x))**(1.0/3)
   return
end
