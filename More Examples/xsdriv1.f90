! Integration of coupled stiff ODE initial value problem
! http://www.cs.yorku.ca/~roumani/fortran/slatecAPI/sdriv1.f.html
program xsdriv1
   implicit none
!
   real , parameter :: ALFA = 1.E0
   integer :: n, lenw
   real :: eps, t, tout
   integer :: i, ierflg, mstate
   real, dimension(:), allocatable :: work, y
   external :: fde
!
   t = 0.00001E0                       ! initial value of independent variable
   n = 3                               ! number of ODE
   lenw = n*n + 11*n + 300             ! workspace needed
   allocate (work(lenw), y(n))
!                                      SET INITIAL CONDITIONS
   y(1) = 10.E0
   y(2) =  0.E0
   y(3) = 10.E0
!                                              PASS PARAMETER
   y(4) = ALFA
   tout = t
   mstate = 1
   eps = .001E0
   do
      call sdriv1(N,t,y,fde,tout,mstate,eps,work,LENW,ierflg)
      if ( mstate>2 ) stop
      write (*,'(4E12.3)') tout , (y(i),i=1,3)
      tout = 10.E0*tout
      if ( tout>=50.E0 ) exit
   enddo
   deallocate(work,y)
end program

subroutine fde(N,T,Y,Ydot)
   implicit none
   integer :: N
   real :: T
   real , dimension(*) :: Y
   real , dimension(*) :: Ydot
   real :: alfa
!
   alfa    = Y(N+1)
   Ydot(1) = 1.E0 + alfa*(Y(2)-Y(1)) - Y(1)*Y(3)
   Ydot(2) = alfa*(Y(1)-Y(2)) - Y(2)*Y(3)
   Ydot(3) = 1.E0 - Y(3)*(Y(1)+Y(2))
   return
end subroutine fde
