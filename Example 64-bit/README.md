# SLATEC using the 64-bit DLL

This example links to a 64-bit SLATEC DLL and calls some SLATEC routines from Fortran. The project includes a PLATO
project file from which you can load the project into PLATO. There is also a batch file you can use
to manually rebuild the example.

## Acknowledgements

These SLATEC DLLs were created by Silverfrost forum user: mecej4
