function i1mach(i) result(s)
implicit none
integer*4 :: i,s,im(16)
data im/5,6,7,6,32,4,2,31,2147483647,2,24, -125, 128, 53, -1021, 1024/
if(i.lt.1.or.i.gt.16)stop 'I1MACH(arg < 1 or arg > 16)'
s=im(i)
return
end function i1mach

real function r1mach(i)
   implicit none
   integer i
   select case (i)
   case (1)
      r1mach = tiny(1.0)
   case (2)
      r1mach = huge(1.0)
   case (3)
      r1mach = epsilon(1.0)/2
   case (4)
      r1mach = epsilon(1.0)
   case (5)
      r1mach = log10(2.0)
   end select
   return
end function r1mach

real*8 function d1mach(i)
   implicit none
   integer i
   select case (i)
      case (1)
         d1mach = tiny(1d0)
      case (2)
         d1mach = huge(1d0)
      case (3)
         d1mach = epsilon(1d0)/2
      case (4)
         d1mach = epsilon(1d0)
      case (5)
         d1mach = log10(2d0)
   end select
   return
end function d1mach


