# Using SLATEC with Silverfrost Fortran

SLATEC Common Mathematical Library is a 'a comprehensive software library containing over 1400 
general purpose mathematical and statistical routines'. Presented here are 32- and 64-bit 
DLLs containing the library that has been compiled for use with FTN95. The original source
can be accessed from here:

https://netlib.org/slatec/

## Additional Documentation

http://www.cs.yorku.ca/~roumani/fortran/slatecAPI/keylist.index.html<br>
http://sdpha2.ucsd.edu/slatec_Source/toc.htm<br>
http://www.lahey.com/docs/lgf14help/LFUGSLATEC.htm
    
## How to use   

There are two DLLs, one each for 32- and -64 bit programs. You need to link the appropriate one
for your code. You will find the DLLs in the 'Example 32-bit' and 'Example 64-bit' folders.
    
## Acknowledgements

These DLLs were created by Silverfrost forum user: mecej4

## Additional

The routines R1MACH, D1MACH and I1MACH have been replaced by their versions in machine.f90.

